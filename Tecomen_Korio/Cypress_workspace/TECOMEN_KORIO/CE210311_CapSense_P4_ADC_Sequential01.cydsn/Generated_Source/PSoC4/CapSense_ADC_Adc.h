/***************************************************************************//**
* \file CapSense_ADC_Adc.h
* \version 4.0
*
* \brief
*   This file provides the sources of APIs specific to the ADC implementation.
*
* \see CapSense P4 v4.0 Datasheet
*
*//*****************************************************************************
* Copyright (2016-2017), Cypress Semiconductor Corporation.
********************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*******************************************************************************/

#if !defined(CY_CAPSENSE_CapSense_ADC_ADC_H)
#define CY_CAPSENSE_CapSense_ADC_ADC_H


#include "cytypes.h"
#include "CapSense_ADC_Structure.h"
#include "CapSense_ADC_Configuration.h"

#if (CapSense_ADC_ADC_EN)
    
/*******************************************************************************
* Function Prototypes 
*******************************************************************************/ 

/**
* \if SECTION_STANDALONE_ADC
* \addtogroup group_adc_public
* \{
*/
#if (CapSense_ADC_ENABLE == CapSense_ADC_ADC_STANDALONE_EN)
    void CapSense_ADC_Start(void);
    void CapSense_ADC_Sleep(void);
    void CapSense_ADC_Wakeup(void);
#endif  /* (CapSense_ADC_ENABLE == CapSense_ADC_ADC_STANDALONE_EN) */
/** \}
* \endif */
/**
* \if SECTION_ADC_PUBLIC
* \addtogroup group_adc_public
* \{
*/
cystatus CapSense_ADC_AdcStartConvert(uint8 chId);
uint8 CapSense_ADC_AdcIsBusy(void);
uint16 CapSense_ADC_AdcReadResult_mVolts(uint8 chId);
uint16 CapSense_ADC_AdcGetResult_mVolts(uint8 chId);
cystatus CapSense_ADC_AdcCalibrate(void);

void CapSense_ADC_AdcStop(void);
void CapSense_ADC_AdcResume(void);
/** \}
* \endif */


CY_ISR_PROTO(CapSense_ADC_AdcIntrHandler);

/**
* \if SECTION_ADC_INTERNAL
* \addtogroup group_adc_internal
* \{
*/

void CapSense_ADC_AdcInitialize(void);
void CapSense_ADC_SetAdcChannel(uint8 chId, uint32 state);
void CapSense_ADC_ConfigAdcResources(void);
void CapSense_ADC_StartAdcFSM(uint32 measureMode);
cystatus CapSense_ADC_AdcCaptureResources(void);
cystatus CapSense_ADC_AdcReleaseResources(void);
void CapSense_ADC_ClearAdcChannels(void);

#define CapSense_ADC_AdcOFFSETNSIZE2MASK(o, s)  (((1uL << (s)) - 1uL) << (o))
/** \}
* \endif */

/**************************************
*           API Constants
**************************************/
#ifdef CYREG_SFLASH_CSDV2_CSD0_ADC_TRIM1
    #define CapSense_ADC_CALIBRATE_VREF                             (CapSense_ADC_ENABLE)
#else
    #define CapSense_ADC_CALIBRATE_VREF                             (CapSense_ADC_DISABLE)
#endif

/* Error value if given bad channel ID. */
#define CapSense_ADC_AdcVALUE_BAD_CHAN_ID            (0x0000FFFFuL)
#define CapSense_ADC_AdcVALUE_BAD_RESULT             (0x0000FFFFuL)
/* Obsolete */
#define CapSense_ADC_AdcVALUE_BADCHANID              (0x0000FFFFuL)
#define CapSense_ADC_AdcVALUE_BADRESULT              (0x0000FFFFuL)

/* Statuses defined for use with IsBusy */
#define CapSense_ADC_AdcSTATUS_LASTCHAN_MASK         (0x0000000FuL)
#define CapSense_ADC_AdcSTATUS_FSM_MASK              (0x000000F0uL)
#define CapSense_ADC_AdcSTATUS_IDLE                  (0x0u)
#define CapSense_ADC_AdcSTATUS_CALIBPH1              (0x10u)
#define CapSense_ADC_AdcSTATUS_CALIBPH2              (0x20u)
#define CapSense_ADC_AdcSTATUS_CALIBPH3              (0x30u)
#define CapSense_ADC_AdcSTATUS_CONVERTING            (0x40u)
#define CapSense_ADC_AdcSTATUS_OVERFLOW              (0x80u)

/* Potential channel states */
#define CapSense_ADC_AdcCHAN_CONNECT                 (1uL)
#define CapSense_ADC_AdcCHAN_DISCONNECT              (0uL)

/* Input/Output constants */
#define CapSense_ADC_AdcHSIOM_PRTSEL_MASK            (0xFuL)
#define CapSense_ADC_AdcHSIOM_PRTSEL_GPIO            (0x0uL)
#define CapSense_ADC_AdcHSIOM_PRTSEL_AMUXBUSB        (0x7uL)

#define CapSense_ADC_AdcGPIO_PC_MASK                 (0x7uL)
#define CapSense_ADC_AdcGPIO_PC_DISCONNECT           (0x0uL)
#define CapSense_ADC_AdcGPIO_PC_INPUT                (0x1uL)

/* Adc Config */
#define CapSense_ADC_AdcCONFIG_FILTER_DELAY_MASK     CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_FILTER_DELAY__OFFSET, \
                                                                        CYFLD_CSD_FILTER_DELAY__SIZE)
#define CapSense_ADC_AdcCONFIG_SHIELD_DELAY_MASK     CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_SHIELD_DELAY__OFFSET, \
                                                                        CYFLD_CSD_SHIELD_DELAY__SIZE)
#define CapSense_ADC_AdcCONFIG_SENSE_EN_MASK         CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_SENSE_EN__OFFSET, \
                                                                        CYFLD_CSD_SENSE_EN__SIZE)
#define CapSense_ADC_AdcCONFIG_CHARGE_MODE_MASK      CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_CHARGE_MODE__OFFSET, \
                                                                        CYFLD_CSD_CHARGE_MODE__SIZE)
#define CapSense_ADC_AdcCONFIG_MUTUAL_CAP_MASK       CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_MUTUAL_CAP__OFFSET, \
                                                                        CYFLD_CSD_MUTUAL_CAP__SIZE)
#define CapSense_ADC_AdcCONFIG_CSX_DUAL_CNT_MASK     CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_CSX_DUAL_CNT__OFFSET, \
                                                                        CYFLD_CSD_CSX_DUAL_CNT__SIZE)
#define CapSense_ADC_AdcCONFIG_DSI_COUNT_SEL_MASK    CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_DSI_COUNT_SEL__OFFSET, \
                                                                        CYFLD_CSD_DSI_COUNT_SEL__SIZE)
#define CapSense_ADC_AdcCONFIG_DSI_SAMPLE_EN_MASK    CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_DSI_SAMPLE_EN__OFFSET, \
                                                                        CYFLD_CSD_DSI_SAMPLE_EN__SIZE)
#define CapSense_ADC_AdcCONFIG_SAMPLE_SYNC_MASK      CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_SAMPLE_SYNC__OFFSET, \
                                                                        CYFLD_CSD_SAMPLE_SYNC__SIZE)
#define CapSense_ADC_AdcCONFIG_DSI_SENSE_EN_MASK     CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_DSI_SENSE_EN__OFFSET, \
                                                                        CYFLD_CSD_DSI_SENSE_EN__SIZE)
#define CapSense_ADC_AdcCONFIG_LP_MODE_MASK          CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_LP_MODE__OFFSET, \
                                                                        CYFLD_CSD_LP_MODE__SIZE)
#define CapSense_ADC_AdcCONFIG_ENABLE_MASK           CapSense_ADC_AdcOFFSETNSIZE2MASK(\
                                                                        CYFLD_CSD_ENABLE__OFFSET, \
                                                                        CYFLD_CSD_ENABLE__SIZE)
#define CapSense_ADC_AdcCONFIG_DEFAULT               (CapSense_ADC_AdcCONFIG_ENABLE_MASK | \
                                                                        (2uL << CYFLD_CSD_FILTER_DELAY__OFFSET) | \
                                                                        CapSense_ADC_AdcCONFIG_SAMPLE_SYNC_MASK | \
                                                                        CapSense_ADC_AdcCONFIG_SENSE_EN_MASK | \
                                                                        CapSense_ADC_AdcCONFIG_DSI_COUNT_SEL_MASK)

/* Measurement modes */
#define CapSense_ADC_AdcMEASMODE_SHIFT               (CYFLD_CSD_ADC_MODE__OFFSET)
#define CapSense_ADC_AdcMEASMODE_MASK                (0x3uL << CapSense_ADC_AdcMEASMODE_SHIFT)
#define CapSense_ADC_AdcMEASMODE_OFF                 (0x0uL << CapSense_ADC_AdcMEASMODE_SHIFT)
#define CapSense_ADC_AdcMEASMODE_VREF                (0x1uL << CapSense_ADC_AdcMEASMODE_SHIFT)
#define CapSense_ADC_AdcMEASMODE_VREFBY2             (0x2uL << CapSense_ADC_AdcMEASMODE_SHIFT)
#define CapSense_ADC_AdcMEASMODE_VIN                 (0x3uL << CapSense_ADC_AdcMEASMODE_SHIFT)

/* Clock defines */
#define CapSense_ADC_AdcSENSE_DIV_DEFAULT            (0x4uL)
#define CapSense_ADC_AdcTOTAL_CLOCK_DIV              (CapSense_ADC_ADC_MODCLK_DIV_DEFAULT * \
                                                                         CapSense_ADC_AdcSENSE_DIV_DEFAULT)
/* SEQ_TIME field definitions: AZ Time */
#define CapSense_ADC_ADC_MAX_AZ_TIME                                (21u)
#if (CapSense_ADC_ADC_MAX_AZ_TIME < CapSense_ADC_ADC_AZ_TIME)
    #define CapSense_ADC_AdcSEQ_TIME_BASE            (((CYDEV_BCLK__HFCLK__HZ * CapSense_ADC_ADC_MAX_AZ_TIME) / \
                                                                         CapSense_ADC_AdcTOTAL_CLOCK_DIV) / 1000000uL)
#else
    #define CapSense_ADC_AdcSEQ_TIME_BASE            (((CYDEV_BCLK__HFCLK__HZ * CapSense_ADC_ADC_AZ_TIME) / \
                                                                         CapSense_ADC_AdcTOTAL_CLOCK_DIV) / 1000000uL)
#endif

#if (0 == CapSense_ADC_AdcSEQ_TIME_BASE)
    #define CapSense_ADC_AdcSEQ_TIME_DEFAUL          (1u)
#else
    #define CapSense_ADC_AdcSEQ_TIME_DEFAUL          (CapSense_ADC_AdcSEQ_TIME_BASE)
#endif

/* Acquisition time definitions: ADC_CTL */
#define CapSense_ADC_AdcACQUISITION_TIME_US          (10uL)
#define CapSense_ADC_AdcACQUISITION_BASE             ((CapSense_ADC_AdcACQUISITION_TIME_US * \
                                                                        (CYDEV_BCLK__HFCLK__MHZ)) / \
                                                                         CapSense_ADC_AdcTOTAL_CLOCK_DIV)

/* SEQ_START field definitions */
#define CapSense_ADC_AdcFSMSETTING_START_SHIFT       (CYFLD_CSD_START__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_START_MASK        (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_START__OFFSET, CYFLD_CSD_START__SIZE))
#define CapSense_ADC_AdcFSMSETTING_STARTSEQ          (0x1uL << CapSense_ADC_AdcFSMSETTING_START_SHIFT)
#define CapSense_ADC_AdcFSMSETTING_SEQ_MODE_SHIFT    (CYFLD_CSD_SEQ_MODE__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_SEQ_MODE_MASK     (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_SEQ_MODE__OFFSET, CYFLD_CSD_SEQ_MODE__SIZE))
#define CapSense_ADC_AdcFSMSETTING_SEQ_MODE_ADC      (0x0uL)
#define CapSense_ADC_AdcFSMSETTING_SEQ_MODE_COARSE   (0x1uL << CapSense_ADC_AdcFSMSETTING_SEQ_MODE_SHIFT)
#define CapSense_ADC_AdcFSMSETTING_ABORT_SHIFT       (CYFLD_CSD_ABORT__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_ABORT_MASK        (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_ABORT__OFFSET, CYFLD_CSD_ABORT__SIZE))
#define CapSense_ADC_AdcFSMSETTING_ABORT             (0x1uL << CapSense_ADC_AdcFSMSETTING_ABORT_SHIFT)
#define CapSense_ADC_AdcFSMSETTING_DSI_START_EN_SHIFT (CYFLD_CSD_DSI_START_EN__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_DSI_START_EN_MASK (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_DSI_START_EN__OFFSET, CYFLD_CSD_DSI_START_EN__SIZE))
#define CapSense_ADC_AdcFSMSETTING_DSI_START_EN      (0x1uL << CapSense_ADC_AdcFSMSETTING_DSI_START_EN_SHIFT)
#define CapSense_ADC_AdcFSMSETTING_AZ0SKIP_SHIFT     (CYFLD_CSD_AZ0_SKIP__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_AZ0SKIP_MASK      (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_AZ0_SKIP__OFFSET, CYFLD_CSD_AZ0_SKIP__SIZE))
#define CapSense_ADC_AdcFSMSETTING_AZ0SKIP           (0x1uL << CapSense_ADC_AdcFSMSETTING_AZ0SKIP_SHIFT)
#define CapSense_ADC_AdcFSMSETTING_AZ1SKIP_SHIFT     (CYFLD_CSD_AZ1_SKIP__OFFSET)
#define CapSense_ADC_AdcFSMSETTING_AZ1SKIP_MASK      (CapSense_ADC_AdcOFFSETNSIZE2MASK \
                                                                        (CYFLD_CSD_AZ1_SKIP__OFFSET, CYFLD_CSD_AZ1_SKIP__SIZE))
#define CapSense_ADC_AdcFSMSETTING_AZ1SKIP           (0x1uL << CapSense_ADC_AdcFSMSETTING_AZ1SKIP_SHIFT)

#define CapSense_ADC_AdcFSMSETTING_NOAZSKIP          (0uL)
#define CapSense_ADC_AdcFSMSETTING_AZSKIP_DEFAULT    (CapSense_ADC_AdcFSMSETTING_AZ0SKIP \
                                                                        | ((0u != CapSense_ADC_ADC_AZ_EN) \
                                                                        ? 0u \
                                                                        : CapSense_ADC_AdcFSMSETTING_AZ1SKIP))
#define CapSense_ADC_AdcFSMSETTING_DSIIGNORE         (0x00000000uL)
#define CapSense_ADC_AdcFSMSETTING_NOABORT           (0x00000000uL)
#define CapSense_ADC_AdcFSMSETTING_SEQMODE           (0x00000000uL)
#define CapSense_ADC_AdcFSMSETTING_START             (0x00000001uL)

/* Interrupt definitions */
#define CapSense_ADC_AdcINTERRUPT_SHIFT              (CYFLD_CSD_ADC_RES__OFFSET)
#define CapSense_ADC_AdcINTERRUPT_MASK               (0x1uL << CapSense_ADC_AdcINTERRUPT_SHIFT)
#define CapSense_ADC_AdcINTERRUPT_CLEAR              (0x00000000uL)
#define CapSense_ADC_AdcINTERRUPT_SET                (CapSense_ADC_AdcINTERRUPT_MASK)

/* IDACB definitions */
/* The idac configuration for ADC use is mostly static, with only the VAL field
   varying. 
   Dynamic Polarity = 1 << 7
   Polarity (normal) = 0 << 8
   Balance, Leg1, Leg2 modes = don't care.
   DSI Control Enable (no mix) = 0 << 21
   Range (low) = 0 << 22
   Leg1, Leg2 enable = 0
   Leg3 enable = 1 << 26
   */

#define CapSense_ADC_AdcIDACB_CONFIG                 (0x04000080uL)
#define CapSense_ADC_AdcIDACB_VAL_MASK               (0x7FuL)
#define CapSense_ADC_AdcCLK16_MASK                   (0x0000FFFFuL)
#define CapSense_ADC_AdcCLK16VAL_SHIFT               (8uL)
/* Switch definitions */
#define CapSense_ADC_AdcSW_HSP_DEFAULT               (0x10000000uL)
#define CapSense_ADC_AdcSW_HSN_DEFAULT               (0x00100000uL)
#define CapSense_ADC_AdcSW_HSP_GETINPOL              (0x00010000uL)
#define CapSense_ADC_AdcSW_HSN_GETINPOL              (0x01000000uL)
#define CapSense_ADC_AdcSW_SHIELD_DEFAULT            (0x00000000uL)
#define CapSense_ADC_AdcSW_SHIELD_VDDA2CSDBUSB       (0x00000100uL)
#define CapSense_ADC_AdcSW_BYP_DEFAULT               (0x00110000uL)
#define CapSense_ADC_AdcSW_CMPP_DEFAULT              (0x00000000uL)
#define CapSense_ADC_AdcSW_CMPN_DEFAULT              (0x00000000uL)
#define CapSense_ADC_AdcSW_REFGEN_DEFAULT            (0x10000000uL)
#define CapSense_ADC_AdcSW_FWMOD_DEFAULT             (0x01100000uL)
#define CapSense_ADC_AdcSW_FWTANK_DEFAULT            (0x01100000uL)

#define CapSense_ADC_AdcSW_CTANK_PINSHIFT            (9uL)
#define CapSense_ADC_AdcSW_CMOD_PINSHIFT             (6uL)
#define CapSense_ADC_AdcSW_CMOD_PORT_MASK            (0x400uL)
#define CapSense_ADC_AdcSW_DSI_CMOD                  (1uL << 4)
#define CapSense_ADC_AdcSW_DSI_CTANK                 (1uL << 0)

#define CapSense_ADC_AdcLVTHRESH                     (2700uL)

/* Vrefhi is achieved via a gain applied to a source. Source value is usually 1.2V */
#define CapSense_ADC_AdcVREFSRC_MV                   (1200uL)
/* The reference voltage is measured at nominal 2400 mV. Measured amount is stored in CYREG_SFLASH_CSDV2_CSD0_ADC_TRIM1 */
#define CapSense_ADC_AdcVREFCALIB_BASE               (2400uL)

/* RefGen settings */
#define CapSense_ADC_AdcREFGEN_GAIN_SHIFT            (CYFLD_CSD_GAIN__OFFSET)

/* At low voltage, REFGEN is enabled and bypassed. */
#define CapSense_ADC_AdcREFGEN_LV                    (0x00000011uL)
#define CapSense_ADC_AdcSW_AMUBUF_LV                 (0x01000100uL)
#define CapSense_ADC_AdcAMBUF_LV                     (0x00000002uL)
/* At normal voltage, REFGEN uses customizer-defined gain */
#define CapSense_ADC_AdcREFGEN_NORM                  (0x00000041uL | \
                                                                        (CapSense_ADC_ADC_GAIN << \
                                                                        CapSense_ADC_AdcREFGEN_GAIN_SHIFT))

#define CapSense_ADC_AdcSW_AMUBUF_NORM               (0x00000000uL)

#if (CapSense_ADC_ENABLE == CapSense_ADC_ADC_STANDALONE_EN)
    #define CapSense_ADC_AdcMODCLK_CMD_DIV_SHIFT     (0u)
    #define CapSense_ADC_AdcMODCLK_CMD_PA_DIV_SHIFT  (8u)
    #define CapSense_ADC_AdcMODCLK_CMD_DISABLE_SHIFT (30u)
    #define CapSense_ADC_AdcMODCLK_CMD_ENABLE_SHIFT  (31u)
    /* No ADC prefix */
    #define CapSense_ADC_AdcMODCLK_CMD_DISABLE_MASK  ((uint32)((uint32)1u << CapSense_ADC_AdcMODCLK_CMD_DISABLE_SHIFT))
    #define CapSense_ADC_AdcMODCLK_CMD_ENABLE_MASK   ((uint32)((uint32)1u << CapSense_ADC_AdcMODCLK_CMD_ENABLE_SHIFT))

    #define CapSense_ADC_AdcMODCLK_DIV_REG           (*(reg32 *)CapSense_ADC_ModClk__DIV_REGISTER)
    #define CapSense_ADC_AdcMODCLK_DIV_PTR           ( (reg32 *)CapSense_ADC_ModClk__DIV_REGISTER)
    #define CapSense_ADC_AdcMODCLK_CMD_REG           (*(reg32 *)CYREG_PERI_DIV_CMD)
    #define CapSense_ADC_AdcMODCLK_CMD_PTR           ( (reg32 *)CYREG_PERI_DIV_CMD)
#endif /* (CapSense_ADC_ENABLE == CapSense_ADC_ADC_STANDALONE_EN) */

/* HSCOMP definitions */
#define CapSense_ADC_AdcHSCMP_AZ_SHIFT               (31uL)
#define CapSense_ADC_AdcHSCMP_AZ_DEFAULT             (0x00000001uL | \
                                                                        (CapSense_ADC_ADC_AZ_EN << \
                                                                        CapSense_ADC_AdcHSCMP_AZ_SHIFT))

#define CapSense_ADC_AdcSTATUS_HSCMP_OUT_MASK        (0x1uL << 2uL)

/* ADC_RES definitions */
#define CapSense_ADC_AdcRES_MAX                      ((1uL << CapSense_ADC_ADC_RESOLUTION) - 1uL)
#define CapSense_ADC_AdcADC_RES_OVERFLOW_MASK        (0x40000000uL)
#define CapSense_ADC_AdcADC_RES_ABORT_MASK           (0x80000000uL)
#define CapSense_ADC_AdcADC_RES_HSCMPPOL_MASK        (0x00010000uL)
#define CapSense_ADC_AdcADC_RES_VALUE_MASK           (0x0000FFFFuL)
#define CapSense_ADC_AdcUNDERFLOW_LIMIT              (8000u)

/* Actual VREFHI is used in calculations. */
#define CapSense_ADC_AdcVREFHI_MV                    (CapSense_ADC_ADC_VREF_MV)

/* Register definitions */
#define CapSense_ADC_AdcCONFIG_REG                   (*(reg32 *) CYREG_CSD_CONFIG)
#define CapSense_ADC_AdcSPARE_REG                    (*(reg32 *) CYREG_CSD_SPARE)
#define CapSense_ADC_AdcSTATUS_REG                   (*(reg32 *) CYREG_CSD_STATUS)
#define CapSense_ADC_AdcSTAT_SEQ_REG                 (*(reg32 *) CYREG_CSD_STAT_SEQ)
#define CapSense_ADC_AdcSTAT_CNTS_REG                (*(reg32 *) CYREG_CSD_STAT_CNTS)
#define CapSense_ADC_AdcRESULT_VAL1_REG              (*(reg32 *) CYREG_CSD_RESULT_VAL1)
#define CapSense_ADC_AdcRESULT_VAL2_REG              (*(reg32 *) CYREG_CSD_RESULT_VAL2)
#define CapSense_ADC_AdcADC_RES_REG                  (*(reg32 *) CYREG_CSD_ADC_RES)
#define CapSense_ADC_AdcINTR_REG                     (*(reg32 *) CYREG_CSD_INTR)
#define CapSense_ADC_AdcINTR_SET_REG                 (*(reg32 *) CYREG_CSD_INTR_SET)
#define CapSense_ADC_AdcINTR_MASK_REG                (*(reg32 *) CYREG_CSD_INTR_MASK)
#define CapSense_ADC_AdcINTR_MASKED_REG              (*(reg32 *) CYREG_CSD_INTR_MASKED)
#define CapSense_ADC_AdcHSCMP_REG                    (*(reg32 *) CYREG_CSD_HSCMP)
#define CapSense_ADC_AdcAMBUF_REG                    (*(reg32 *) CYREG_CSD_AMBUF)
#define CapSense_ADC_AdcREFGEN_REG                   (*(reg32 *) CYREG_CSD_REFGEN)
#define CapSense_ADC_AdcCSDCMP_REG                   (*(reg32 *) CYREG_CSD_CSDCMP)
#define CapSense_ADC_AdcIDACA_REG                    (*(reg32 *) CYREG_CSD_IDACA)
#define CapSense_ADC_AdcIDACB_REG                    (*(reg32 *) CYREG_CSD_IDACB)
#define CapSense_ADC_AdcSW_RES_REG                   (*(reg32 *) CYREG_CSD_SW_RES)
#define CapSense_ADC_AdcSENSE_PERIOD_REG             (*(reg32 *) CYREG_CSD_SENSE_PERIOD)
#define CapSense_ADC_AdcSENSE_DUTY_REG               (*(reg32 *) CYREG_CSD_SENSE_DUTY)
#define CapSense_ADC_AdcSW_HS_P_SEL_REG              (*(reg32 *) CYREG_CSD_SW_HS_P_SEL)
#define CapSense_ADC_AdcSW_HS_N_SEL_REG              (*(reg32 *) CYREG_CSD_SW_HS_N_SEL)
#define CapSense_ADC_AdcSW_SHIELD_SEL_REG            (*(reg32 *) CYREG_CSD_SW_SHIELD_SEL)
#define CapSense_ADC_AdcSW_AMUXBUF_SEL_REG           (*(reg32 *) CYREG_CSD_SW_AMUXBUF_SEL)
#define CapSense_ADC_AdcSW_BYP_SEL_REG               (*(reg32 *) CYREG_CSD_SW_BYP_SEL)
#define CapSense_ADC_AdcSW_CMP_P_SEL_REG             (*(reg32 *) CYREG_CSD_SW_CMP_P_SEL)
#define CapSense_ADC_AdcSW_CMP_N_SEL_REG             (*(reg32 *) CYREG_CSD_SW_CMP_N_SEL)
#define CapSense_ADC_AdcSW_REFGEN_SEL_REG            (*(reg32 *) CYREG_CSD_SW_REFGEN_SEL)
#define CapSense_ADC_AdcSW_FW_MOD_SEL_REG            (*(reg32 *) CYREG_CSD_SW_FW_MOD_SEL)
#define CapSense_ADC_AdcSW_FW_TANK_SEL_REG           (*(reg32 *) CYREG_CSD_SW_FW_TANK_SEL)
#define CapSense_ADC_AdcSW_DSI_SEL_REG               (*(reg32 *) CYREG_CSD_SW_DSI_SEL)
#define CapSense_ADC_AdcSEQ_TIME_REG                 (*(reg32 *) CYREG_CSD_SEQ_TIME)
#define CapSense_ADC_AdcSEQ_INIT_CNT_REG             (*(reg32 *) CYREG_CSD_SEQ_INIT_CNT)
#define CapSense_ADC_AdcSEQ_NORM_CNT_REG             (*(reg32 *) CYREG_CSD_SEQ_NORM_CNT)
#define CapSense_ADC_AdcADC_CTL_REG                  (*(reg32 *) CYREG_CSD_ADC_CTL)
#define CapSense_ADC_AdcSEQ_START_REG                (*(reg32 *) CYREG_CSD_SEQ_START)

#define CapSense_ADC_AdcCONFIG_PTR                   ((reg32 *) CYREG_CSD_CONFIG)
#define CapSense_ADC_AdcSPARE_PTR                    ((reg32 *) CYREG_CSD_SPARE)
#define CapSense_ADC_AdcSTATUS_PTR                   ((reg32 *) CYREG_CSD_STATUS)
#define CapSense_ADC_AdcSTAT_SEQ_PTR                 ((reg32 *) CYREG_CSD_STAT_SEQ)
#define CapSense_ADC_AdcSTAT_CNTS_PTR                ((reg32 *) CYREG_CSD_STAT_CNTS)
#define CapSense_ADC_AdcRESULT_VAL1_PTR              ((reg32 *) CYREG_CSD_RESULT_VAL1)
#define CapSense_ADC_AdcRESULT_VAL2_PTR              ((reg32 *) CYREG_CSD_RESULT_VAL2)
#define CapSense_ADC_AdcADC_RES_PTR                  ((reg32 *) CYREG_CSD_ADC_RES)
#define CapSense_ADC_AdcINTR_PTR                     ((reg32 *) CYREG_CSD_INTR)
#define CapSense_ADC_AdcINTR_SET_PTR                 ((reg32 *) CYREG_CSD_INTR_SET)
#define CapSense_ADC_AdcINTR_MASK_PTR                ((reg32 *) CYREG_CSD_INTR_MASK)
#define CapSense_ADC_AdcINTR_MASKED_PTR              ((reg32 *) CYREG_CSD_INTR_MASKED)
#define CapSense_ADC_AdcHSCMP_PTR                    ((reg32 *) CYREG_CSD_HSCMP)
#define CapSense_ADC_AdcAMBUF_PTR                    ((reg32 *) CYREG_CSD_AMBUF)
#define CapSense_ADC_AdcREFGEN_PTR                   ((reg32 *) CYREG_CSD_REFGEN)
#define CapSense_ADC_AdcCSDCMP_PTR                   ((reg32 *) CYREG_CSD_CSDCMP)
#define CapSense_ADC_AdcIDACA_PTR                    ((reg32 *) CYREG_CSD_IDACA)
#define CapSense_ADC_AdcIDACB_PTR                    ((reg32 *) CYREG_CSD_IDACB)
#define CapSense_ADC_AdcSW_RES_PTR                   ((reg32 *) CYREG_CSD_SW_RES)
#define CapSense_ADC_AdcSENSE_PERIOD_PTR             ((reg32 *) CYREG_CSD_SENSE_PERIOD)
#define CapSense_ADC_AdcSENSE_DUTY_PTR               ((reg32 *) CYREG_CSD_SENSE_DUTY)
#define CapSense_ADC_AdcSW_HS_P_SEL_PTR              ((reg32 *) CYREG_CSD_SW_HS_P_SEL)
#define CapSense_ADC_AdcSW_HS_N_SEL_PTR              ((reg32 *) CYREG_CSD_SW_HS_N_SEL)
#define CapSense_ADC_AdcSW_SHIELD_SEL_PTR            ((reg32 *) CYREG_CSD_SW_SHIELD_SEL)
#define CapSense_ADC_AdcSW_AMUXBUF_SEL_PTR           ((reg32 *) CYREG_CSD_SW_AMUXBUF_SEL)
#define CapSense_ADC_AdcSW_BYP_SEL_PTR               ((reg32 *) CYREG_CSD_SW_BYP_SEL)
#define CapSense_ADC_AdcSW_CMP_P_SEL_PTR             ((reg32 *) CYREG_CSD_SW_CMP_P_SEL)
#define CapSense_ADC_AdcSW_CMP_N_SEL_PTR             ((reg32 *) CYREG_CSD_SW_CMP_N_SEL)
#define CapSense_ADC_AdcSW_REFGEN_SEL_PTR            ((reg32 *) CYREG_CSD_SW_REFGEN_SEL)
#define CapSense_ADC_AdcSW_FW_MOD_SEL_PTR            ((reg32 *) CYREG_CSD_SW_FW_MOD_SEL)
#define CapSense_ADC_AdcSW_FW_TANK_SEL_PTR           ((reg32 *) CYREG_CSD_SW_FW_TANK_SEL)
#define CapSense_ADC_AdcSW_DSI_SEL_PTR               ((reg32 *) CYREG_CSD_SW_DSI_SEL)
#define CapSense_ADC_AdcSEQ_TIME_PTR                 ((reg32 *) CYREG_CSD_SEQ_TIME)
#define CapSense_ADC_AdcSEQ_INIT_CNT_PTR             ((reg32 *) CYREG_CSD_SEQ_INIT_CNT)
#define CapSense_ADC_AdcSEQ_NORM_CNT_PTR             ((reg32 *) CYREG_CSD_SEQ_NORM_CNT)
#define CapSense_ADC_AdcADC_CTL_PTR                  ((reg32 *) CYREG_CSD_ADC_CTL)
#define CapSense_ADC_AdcSEQ_START_PTR                ((reg32 *) CYREG_CSD_SEQ_START)


#endif 	/* CapSense_ADC_ADC_EN */

#endif	/* CY_CAPSENSE_CapSense_ADC_ADC_H */


/* [] END OF FILE */

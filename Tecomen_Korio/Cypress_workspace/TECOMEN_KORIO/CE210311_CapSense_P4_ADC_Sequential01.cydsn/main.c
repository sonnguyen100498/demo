/*****************************************************************************
* File Name: main.c
*
* Version: 1.10
*
* Description: This code example demonstrates the use of CapSense_ADC Component 
* for scanning a CapSense sensor and measuring the input voltage on any
* input pin. 
*
* Related Document: CE210311.pdf
*
* Hardware Dependency: See code example document CE210311
*
******************************************************************************
* Copyright (2016), Cypress Semiconductor Corporation.
******************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*****************************************************************************/
#include <project.h>

/*******************************************************************************
*   Included Headers
*******************************************************************************/
#include <stdio.h>
/* Converting integer to ASCII */
#define CONVERT_TO_ASCII        (0x30u)

/* Character to clear screen on hyperterminal */
#define CLEAR_SCREEN            (0x0Cu)

/* Delay parameter */
#define DELAY_MS                (20u)

/* Maximum PWM Compare value */
#define PWM_MAX                 (0xFFFFu)

/* Minimum PWM Compare value */
#define PWM_MIN                 (0x0000u)

/* Maximum input voltage value in mV. This value is equal
*  to the VDDA specified in the CYDWR tab
*/
#define VMAX_MV                 (CYDEV_VDDA_MV)

/* Minimum input voltage value in mV */
#define VMIN_MV                 (100u)

/*******************************************************************************
*  Function Declarations
*******************************************************************************/
/* Function prototype for displaying ADC conversion value on hyperterminal and
   control LED brightness depending on the input voltage value */
void ADC_DisplayState(void);

/*******************************************************************************
*   Module Variable and Constant Declarations with Applicable Initializations
*******************************************************************************/

/* Definition for operating mode */
typedef enum
{
    STATE_CAPSENSE = 0x00u,
    STATE_ADC      = 0x01u
}SCAN_MODE;

/* Variable to store the next state */
SCAN_MODE scanState = STATE_CAPSENSE;

/* ADC conversion value */
uint32 mVolts = 0;

/* PWM compare value to control LED brightness */
uint32 pwmCompareCount = 0;

/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  main() performs the following functions:
*  1. Initializes CapSense and SCB blocks
*  2. Scans & processes single button sensor
*  3. Measure input voltage on input pin after CapSense scanning is completed
*
*  The CapSense and ADC operations are time multiplexed. After CapSense scanning 
*  is completed input voltage measurement is initiated
*******************************************************************************/
int main()
{
    /* Enable Global interrupts for CapSense operation */
    CyGlobalIntEnable;
         
                         
    /* Start UART block */
    UART_Start();
  
    /* Start CapSense_ADC component */
    CapSense_ADC_Start();
          
    /* Scan the CapSense sensor */
    CapSense_ADC_ScanAllWidgets();
        
    /* Set the next scan state to ADC to perform voltage measurement */
    scanState = STATE_ADC;
    char str[50];
    for(;;)
    {
        /* Initiate the CapSense or ADC scan only when previous 
           CapSense or ADC process is completed */
        if((CapSense_ADC_NOT_BUSY == CapSense_ADC_IsBusy()) && (CapSense_ADC_AdcSTATUS_IDLE == CapSense_ADC_AdcIsBusy()))
        {
            /* If scan state is CapSense, initiate CapSense scan and 
               process last cycle ADC data 
            */
            if(scanState == STATE_CAPSENSE)
            {
                /* Update CapSense parameters set via CapSense Tuner */
//                CapSense_ADC_RunTuner();
                
                /* Scan configured sensor */
                CapSense_ADC_ScanAllWidgets();
                /* Process previous cycle CapSense Data */
                CapSense_ADC_ProcessAllWidgets();
                /* Read the measured voltage value from last ADC cycle */
                mVolts = CapSense_ADC_AdcGetResult_mVolts(CapSense_ADC_AdcCHANNEL_0);
                
                /* Control LED brightness depending on the input voltage value and shows
                   ADC conversion value on hyperterminal */
//                ADC_DisplayState();
                sprintf(str, "voltage: %ld\n", mVolts);
                UART_PutString(str);
          
                                                
                /* Configure next scan state to ADC to perform input voltage measurement */
                scanState = STATE_ADC;
            }
            else if (scanState == STATE_ADC)
            {
                /* Start an ADC Conversion conversion */
                CapSense_ADC_AdcStartConvert(CapSense_ADC_AdcCHANNEL_0);
              
//                /* Process previous cycle CapSense Data */
//                CapSense_ADC_ProcessAllWidgets();
                
                /* Set variable to scan CapSense sensor in the next cycle */
                scanState = STATE_CAPSENSE;
            }
        }
        
        /* Delay added to sample input voltage every 100 milliseconds */
        CyDelay(100);
    }
}

/*******************************************************************************
* Function Name: ADC_DisplayState
********************************************************************************
* Summary:
*  Changes LEDs brightness by changing the updating the compare value of the PWM.
*  The compare value is computed depending on the value of input voltage.
*  Display ADC conversion data on hyperterminal
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/



/* [] END OF FILE */

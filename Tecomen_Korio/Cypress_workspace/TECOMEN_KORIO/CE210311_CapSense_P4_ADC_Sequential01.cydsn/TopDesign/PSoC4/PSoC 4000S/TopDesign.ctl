-- =========================================​====================================
-- The following directives assign pins to the locations specific for the
-- PSoC 4000S device.
-- =========================================​====================================
 
-- === CapSense_ADC ===
attribute port_location of \CapSense_ADC:AdcInput(0)\ : label is "PORT(2,4)";
attribute port_location of \CapSense_ADC:Cmod(0)\ : label is "PORT(4,1)";
attribute port_location of \CapSense_ADC:Sns(0)\ : label is "PORT(0,1)";

-- === I2C ===
attribute port_location of \EZI2C:scl(0)\ : label is "PORT(3,0)";
attribute port_location of \EZI2C:sda(0)\ : label is "PORT(3,1)";
 
-- === UART ===
attribute port_location of \UART:tx(0)\ : label is "PORT(0,5)";
 
-- === LED_RED ===
attribute port_location of LED_RED(0) : label is "PORT(3,4)";
/***************************************************************************//**
* \file CapSense_ADC_RegisterMap.h
* \version 4.0
*
* \brief
*   This file provides the definitions for the component data structure register.
*
* \see CapSense P4 v4.0 Datasheet
*
*//*****************************************************************************
* Copyright (2016-2017), Cypress Semiconductor Corporation.
********************************************************************************
* This software is owned by Cypress Semiconductor Corporation (Cypress) and is
* protected by and subject to worldwide patent protection (United States and
* foreign), United States copyright laws and international treaty provisions.
* Cypress hereby grants to licensee a personal, non-exclusive, non-transferable
* license to copy, use, modify, create derivative works of, and compile the
* Cypress Source Code and derivative works for the sole purpose of creating
* custom software in support of licensee product to be used only in conjunction
* with a Cypress integrated circuit as specified in the applicable agreement.
* Any reproduction, modification, translation, compilation, or representation of
* this software except as specified above is prohibited without the express
* written permission of Cypress.
*
* Disclaimer: CYPRESS MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH
* REGARD TO THIS MATERIAL, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
* Cypress reserves the right to make changes without further notice to the
* materials described herein. Cypress does not assume any liability arising out
* of the application or use of any product or circuit described herein. Cypress
* does not authorize its products for use as critical components in life-support
* systems where a malfunction or failure may reasonably be expected to result in
* significant injury to the user. The inclusion of Cypress' product in a life-
* support systems application implies that the manufacturer assumes all risk of
* such use and in doing so indemnifies Cypress against all charges. Use may be
* limited by and subject to the applicable Cypress software license agreement.
*******************************************************************************/

#if !defined(CY_CAPSENSE_CapSense_ADC_REGISTER_MAP_H)
#define CY_CAPSENSE_CapSense_ADC_REGISTER_MAP_H

#include <cytypes.h>
#include "CapSense_ADC_Configuration.h"
#include "CapSense_ADC_Structure.h"

/*****************************************************************************/
/* RAM Data structure register definitions                                   */
/*****************************************************************************/
#define CapSense_ADC_CONFIG_ID_VALUE                        (CapSense_ADC_dsRam.configId)
#define CapSense_ADC_CONFIG_ID_OFFSET                       (0u)
#define CapSense_ADC_CONFIG_ID_SIZE                         (2u)
#define CapSense_ADC_CONFIG_ID_PARAM_ID                     (0x87000000u)

#define CapSense_ADC_DEVICE_ID_VALUE                        (CapSense_ADC_dsRam.deviceId)
#define CapSense_ADC_DEVICE_ID_OFFSET                       (2u)
#define CapSense_ADC_DEVICE_ID_SIZE                         (2u)
#define CapSense_ADC_DEVICE_ID_PARAM_ID                     (0x8B000002u)

#define CapSense_ADC_TUNER_CMD_VALUE                        (CapSense_ADC_dsRam.tunerCmd)
#define CapSense_ADC_TUNER_CMD_OFFSET                       (4u)
#define CapSense_ADC_TUNER_CMD_SIZE                         (2u)
#define CapSense_ADC_TUNER_CMD_PARAM_ID                     (0xAD000004u)

#define CapSense_ADC_SCAN_COUNTER_VALUE                     (CapSense_ADC_dsRam.scanCounter)
#define CapSense_ADC_SCAN_COUNTER_OFFSET                    (6u)
#define CapSense_ADC_SCAN_COUNTER_SIZE                      (2u)
#define CapSense_ADC_SCAN_COUNTER_PARAM_ID                  (0x8A000006u)

#define CapSense_ADC_STATUS_VALUE                           (CapSense_ADC_dsRam.status)
#define CapSense_ADC_STATUS_OFFSET                          (8u)
#define CapSense_ADC_STATUS_SIZE                            (4u)
#define CapSense_ADC_STATUS_PARAM_ID                        (0xCA000008u)

#define CapSense_ADC_WDGT_ENABLE0_VALUE                     (CapSense_ADC_dsRam.wdgtEnable[0u])
#define CapSense_ADC_WDGT_ENABLE0_OFFSET                    (12u)
#define CapSense_ADC_WDGT_ENABLE0_SIZE                      (4u)
#define CapSense_ADC_WDGT_ENABLE0_PARAM_ID                  (0xE000000Cu)

#define CapSense_ADC_WDGT_STATUS0_VALUE                     (CapSense_ADC_dsRam.wdgtStatus[0u])
#define CapSense_ADC_WDGT_STATUS0_OFFSET                    (16u)
#define CapSense_ADC_WDGT_STATUS0_SIZE                      (4u)
#define CapSense_ADC_WDGT_STATUS0_PARAM_ID                  (0xCD000010u)

#define CapSense_ADC_SNS_STATUS0_VALUE                      (CapSense_ADC_dsRam.snsStatus[0u])
#define CapSense_ADC_SNS_STATUS0_OFFSET                     (20u)
#define CapSense_ADC_SNS_STATUS0_SIZE                       (1u)
#define CapSense_ADC_SNS_STATUS0_PARAM_ID                   (0x4B000014u)

#define CapSense_ADC_SNS_STATUS1_VALUE                      (CapSense_ADC_dsRam.snsStatus[1u])
#define CapSense_ADC_SNS_STATUS1_OFFSET                     (21u)
#define CapSense_ADC_SNS_STATUS1_SIZE                       (1u)
#define CapSense_ADC_SNS_STATUS1_PARAM_ID                   (0x4D000015u)

#define CapSense_ADC_SNS_STATUS2_VALUE                      (CapSense_ADC_dsRam.snsStatus[2u])
#define CapSense_ADC_SNS_STATUS2_OFFSET                     (22u)
#define CapSense_ADC_SNS_STATUS2_SIZE                       (1u)
#define CapSense_ADC_SNS_STATUS2_PARAM_ID                   (0x47000016u)

#define CapSense_ADC_SNS_STATUS3_VALUE                      (CapSense_ADC_dsRam.snsStatus[3u])
#define CapSense_ADC_SNS_STATUS3_OFFSET                     (23u)
#define CapSense_ADC_SNS_STATUS3_SIZE                       (1u)
#define CapSense_ADC_SNS_STATUS3_PARAM_ID                   (0x41000017u)

#define CapSense_ADC_ADC_RESULT0_VALUE                      (CapSense_ADC_dsRam.adcResult[0u])
#define CapSense_ADC_ADC_RESULT0_OFFSET                     (24u)
#define CapSense_ADC_ADC_RESULT0_SIZE                       (2u)
#define CapSense_ADC_ADC_RESULT0_PARAM_ID                   (0x80000018u)

#define CapSense_ADC_ADC_CODE0_VALUE                        (CapSense_ADC_dsRam.adcCode[0u])
#define CapSense_ADC_ADC_CODE0_OFFSET                       (26u)
#define CapSense_ADC_ADC_CODE0_SIZE                         (2u)
#define CapSense_ADC_ADC_CODE0_PARAM_ID                     (0x8C00001Au)

#define CapSense_ADC_ADC_STATUS_VALUE                       (CapSense_ADC_dsRam.adcStatus)
#define CapSense_ADC_ADC_STATUS_OFFSET                      (28u)
#define CapSense_ADC_ADC_STATUS_SIZE                        (1u)
#define CapSense_ADC_ADC_STATUS_PARAM_ID                    (0x4900001Cu)

#define CapSense_ADC_ADC_IDAC_VALUE                         (CapSense_ADC_dsRam.adcIdac)
#define CapSense_ADC_ADC_IDAC_OFFSET                        (29u)
#define CapSense_ADC_ADC_IDAC_SIZE                          (1u)
#define CapSense_ADC_ADC_IDAC_PARAM_ID                      (0x4F00001Du)

#define CapSense_ADC_CSD0_CONFIG_VALUE                      (CapSense_ADC_dsRam.csd0Config)
#define CapSense_ADC_CSD0_CONFIG_OFFSET                     (30u)
#define CapSense_ADC_CSD0_CONFIG_SIZE                       (2u)
#define CapSense_ADC_CSD0_CONFIG_PARAM_ID                   (0xAB80001Eu)

#define CapSense_ADC_MOD_CSD_CLK_VALUE                      (CapSense_ADC_dsRam.modCsdClk)
#define CapSense_ADC_MOD_CSD_CLK_OFFSET                     (32u)
#define CapSense_ADC_MOD_CSD_CLK_SIZE                       (1u)
#define CapSense_ADC_MOD_CSD_CLK_PARAM_ID                   (0x63800020u)

#define CapSense_ADC_ADC_RESOLUTION_VALUE                   (CapSense_ADC_dsRam.adcResolution)
#define CapSense_ADC_ADC_RESOLUTION_OFFSET                  (33u)
#define CapSense_ADC_ADC_RESOLUTION_SIZE                    (1u)
#define CapSense_ADC_ADC_RESOLUTION_PARAM_ID                (0x4E800021u)

#define CapSense_ADC_ADC_AZTIME_VALUE                       (CapSense_ADC_dsRam.adcAzTime)
#define CapSense_ADC_ADC_AZTIME_OFFSET                      (34u)
#define CapSense_ADC_ADC_AZTIME_SIZE                        (1u)
#define CapSense_ADC_ADC_AZTIME_PARAM_ID                    (0x44800022u)

#define CapSense_ADC_BUTTON0_RESOLUTION_VALUE               (CapSense_ADC_dsRam.wdgtList.button0.resolution)
#define CapSense_ADC_BUTTON0_RESOLUTION_OFFSET              (36u)
#define CapSense_ADC_BUTTON0_RESOLUTION_SIZE                (2u)
#define CapSense_ADC_BUTTON0_RESOLUTION_PARAM_ID            (0x81800024u)

#define CapSense_ADC_BUTTON0_FINGER_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button0.fingerTh)
#define CapSense_ADC_BUTTON0_FINGER_TH_OFFSET               (38u)
#define CapSense_ADC_BUTTON0_FINGER_TH_SIZE                 (2u)
#define CapSense_ADC_BUTTON0_FINGER_TH_PARAM_ID             (0x8D800026u)

#define CapSense_ADC_BUTTON0_NOISE_TH_VALUE                 (CapSense_ADC_dsRam.wdgtList.button0.noiseTh)
#define CapSense_ADC_BUTTON0_NOISE_TH_OFFSET                (40u)
#define CapSense_ADC_BUTTON0_NOISE_TH_SIZE                  (1u)
#define CapSense_ADC_BUTTON0_NOISE_TH_PARAM_ID              (0x4A800028u)

#define CapSense_ADC_BUTTON0_NNOISE_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button0.nNoiseTh)
#define CapSense_ADC_BUTTON0_NNOISE_TH_OFFSET               (41u)
#define CapSense_ADC_BUTTON0_NNOISE_TH_SIZE                 (1u)
#define CapSense_ADC_BUTTON0_NNOISE_TH_PARAM_ID             (0x4C800029u)

#define CapSense_ADC_BUTTON0_HYSTERESIS_VALUE               (CapSense_ADC_dsRam.wdgtList.button0.hysteresis)
#define CapSense_ADC_BUTTON0_HYSTERESIS_OFFSET              (42u)
#define CapSense_ADC_BUTTON0_HYSTERESIS_SIZE                (1u)
#define CapSense_ADC_BUTTON0_HYSTERESIS_PARAM_ID            (0x4680002Au)

#define CapSense_ADC_BUTTON0_ON_DEBOUNCE_VALUE              (CapSense_ADC_dsRam.wdgtList.button0.onDebounce)
#define CapSense_ADC_BUTTON0_ON_DEBOUNCE_OFFSET             (43u)
#define CapSense_ADC_BUTTON0_ON_DEBOUNCE_SIZE               (1u)
#define CapSense_ADC_BUTTON0_ON_DEBOUNCE_PARAM_ID           (0x4080002Bu)

#define CapSense_ADC_BUTTON0_LOW_BSLN_RST_VALUE             (CapSense_ADC_dsRam.wdgtList.button0.lowBslnRst)
#define CapSense_ADC_BUTTON0_LOW_BSLN_RST_OFFSET            (44u)
#define CapSense_ADC_BUTTON0_LOW_BSLN_RST_SIZE              (1u)
#define CapSense_ADC_BUTTON0_LOW_BSLN_RST_PARAM_ID          (0x4B80002Cu)

#define CapSense_ADC_BUTTON0_IDAC_MOD0_VALUE                (CapSense_ADC_dsRam.wdgtList.button0.idacMod[0u])
#define CapSense_ADC_BUTTON0_IDAC_MOD0_OFFSET               (45u)
#define CapSense_ADC_BUTTON0_IDAC_MOD0_SIZE                 (1u)
#define CapSense_ADC_BUTTON0_IDAC_MOD0_PARAM_ID             (0x4000002Du)

#define CapSense_ADC_BUTTON0_SNS_CLK_VALUE                  (CapSense_ADC_dsRam.wdgtList.button0.snsClk)
#define CapSense_ADC_BUTTON0_SNS_CLK_OFFSET                 (46u)
#define CapSense_ADC_BUTTON0_SNS_CLK_SIZE                   (2u)
#define CapSense_ADC_BUTTON0_SNS_CLK_PARAM_ID               (0x8F80002Eu)

#define CapSense_ADC_BUTTON0_SNS_CLK_SOURCE_VALUE           (CapSense_ADC_dsRam.wdgtList.button0.snsClkSource)
#define CapSense_ADC_BUTTON0_SNS_CLK_SOURCE_OFFSET          (48u)
#define CapSense_ADC_BUTTON0_SNS_CLK_SOURCE_SIZE            (1u)
#define CapSense_ADC_BUTTON0_SNS_CLK_SOURCE_PARAM_ID        (0x4D800030u)

#define CapSense_ADC_BUTTON0_FINGER_CAP_VALUE               (CapSense_ADC_dsRam.wdgtList.button0.fingerCap)
#define CapSense_ADC_BUTTON0_FINGER_CAP_OFFSET              (50u)
#define CapSense_ADC_BUTTON0_FINGER_CAP_SIZE                (2u)
#define CapSense_ADC_BUTTON0_FINGER_CAP_PARAM_ID            (0xAF000032u)

#define CapSense_ADC_BUTTON0_SIGPFC_VALUE                   (CapSense_ADC_dsRam.wdgtList.button0.sigPFC)
#define CapSense_ADC_BUTTON0_SIGPFC_OFFSET                  (52u)
#define CapSense_ADC_BUTTON0_SIGPFC_SIZE                    (2u)
#define CapSense_ADC_BUTTON0_SIGPFC_PARAM_ID                (0xA2000034u)

#define CapSense_ADC_BUTTON1_RESOLUTION_VALUE               (CapSense_ADC_dsRam.wdgtList.button1.resolution)
#define CapSense_ADC_BUTTON1_RESOLUTION_OFFSET              (54u)
#define CapSense_ADC_BUTTON1_RESOLUTION_SIZE                (2u)
#define CapSense_ADC_BUTTON1_RESOLUTION_PARAM_ID            (0x8B810036u)

#define CapSense_ADC_BUTTON1_FINGER_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button1.fingerTh)
#define CapSense_ADC_BUTTON1_FINGER_TH_OFFSET               (56u)
#define CapSense_ADC_BUTTON1_FINGER_TH_SIZE                 (2u)
#define CapSense_ADC_BUTTON1_FINGER_TH_PARAM_ID             (0x84810038u)

#define CapSense_ADC_BUTTON1_NOISE_TH_VALUE                 (CapSense_ADC_dsRam.wdgtList.button1.noiseTh)
#define CapSense_ADC_BUTTON1_NOISE_TH_OFFSET                (58u)
#define CapSense_ADC_BUTTON1_NOISE_TH_SIZE                  (1u)
#define CapSense_ADC_BUTTON1_NOISE_TH_PARAM_ID              (0x4081003Au)

#define CapSense_ADC_BUTTON1_NNOISE_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button1.nNoiseTh)
#define CapSense_ADC_BUTTON1_NNOISE_TH_OFFSET               (59u)
#define CapSense_ADC_BUTTON1_NNOISE_TH_SIZE                 (1u)
#define CapSense_ADC_BUTTON1_NNOISE_TH_PARAM_ID             (0x4681003Bu)

#define CapSense_ADC_BUTTON1_HYSTERESIS_VALUE               (CapSense_ADC_dsRam.wdgtList.button1.hysteresis)
#define CapSense_ADC_BUTTON1_HYSTERESIS_OFFSET              (60u)
#define CapSense_ADC_BUTTON1_HYSTERESIS_SIZE                (1u)
#define CapSense_ADC_BUTTON1_HYSTERESIS_PARAM_ID            (0x4D81003Cu)

#define CapSense_ADC_BUTTON1_ON_DEBOUNCE_VALUE              (CapSense_ADC_dsRam.wdgtList.button1.onDebounce)
#define CapSense_ADC_BUTTON1_ON_DEBOUNCE_OFFSET             (61u)
#define CapSense_ADC_BUTTON1_ON_DEBOUNCE_SIZE               (1u)
#define CapSense_ADC_BUTTON1_ON_DEBOUNCE_PARAM_ID           (0x4B81003Du)

#define CapSense_ADC_BUTTON1_LOW_BSLN_RST_VALUE             (CapSense_ADC_dsRam.wdgtList.button1.lowBslnRst)
#define CapSense_ADC_BUTTON1_LOW_BSLN_RST_OFFSET            (62u)
#define CapSense_ADC_BUTTON1_LOW_BSLN_RST_SIZE              (1u)
#define CapSense_ADC_BUTTON1_LOW_BSLN_RST_PARAM_ID          (0x4181003Eu)

#define CapSense_ADC_BUTTON1_IDAC_MOD0_VALUE                (CapSense_ADC_dsRam.wdgtList.button1.idacMod[0u])
#define CapSense_ADC_BUTTON1_IDAC_MOD0_OFFSET               (63u)
#define CapSense_ADC_BUTTON1_IDAC_MOD0_SIZE                 (1u)
#define CapSense_ADC_BUTTON1_IDAC_MOD0_PARAM_ID             (0x4A01003Fu)

#define CapSense_ADC_BUTTON1_SNS_CLK_VALUE                  (CapSense_ADC_dsRam.wdgtList.button1.snsClk)
#define CapSense_ADC_BUTTON1_SNS_CLK_OFFSET                 (64u)
#define CapSense_ADC_BUTTON1_SNS_CLK_SIZE                   (2u)
#define CapSense_ADC_BUTTON1_SNS_CLK_PARAM_ID               (0x84810040u)

#define CapSense_ADC_BUTTON1_SNS_CLK_SOURCE_VALUE           (CapSense_ADC_dsRam.wdgtList.button1.snsClkSource)
#define CapSense_ADC_BUTTON1_SNS_CLK_SOURCE_OFFSET          (66u)
#define CapSense_ADC_BUTTON1_SNS_CLK_SOURCE_SIZE            (1u)
#define CapSense_ADC_BUTTON1_SNS_CLK_SOURCE_PARAM_ID        (0x40810042u)

#define CapSense_ADC_BUTTON1_FINGER_CAP_VALUE               (CapSense_ADC_dsRam.wdgtList.button1.fingerCap)
#define CapSense_ADC_BUTTON1_FINGER_CAP_OFFSET              (68u)
#define CapSense_ADC_BUTTON1_FINGER_CAP_SIZE                (2u)
#define CapSense_ADC_BUTTON1_FINGER_CAP_PARAM_ID            (0xA3010044u)

#define CapSense_ADC_BUTTON1_SIGPFC_VALUE                   (CapSense_ADC_dsRam.wdgtList.button1.sigPFC)
#define CapSense_ADC_BUTTON1_SIGPFC_OFFSET                  (70u)
#define CapSense_ADC_BUTTON1_SIGPFC_SIZE                    (2u)
#define CapSense_ADC_BUTTON1_SIGPFC_PARAM_ID                (0xAF010046u)

#define CapSense_ADC_BUTTON2_RESOLUTION_VALUE               (CapSense_ADC_dsRam.wdgtList.button2.resolution)
#define CapSense_ADC_BUTTON2_RESOLUTION_OFFSET              (72u)
#define CapSense_ADC_BUTTON2_RESOLUTION_SIZE                (2u)
#define CapSense_ADC_BUTTON2_RESOLUTION_PARAM_ID            (0x83820048u)

#define CapSense_ADC_BUTTON2_FINGER_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button2.fingerTh)
#define CapSense_ADC_BUTTON2_FINGER_TH_OFFSET               (74u)
#define CapSense_ADC_BUTTON2_FINGER_TH_SIZE                 (2u)
#define CapSense_ADC_BUTTON2_FINGER_TH_PARAM_ID             (0x8F82004Au)

#define CapSense_ADC_BUTTON2_NOISE_TH_VALUE                 (CapSense_ADC_dsRam.wdgtList.button2.noiseTh)
#define CapSense_ADC_BUTTON2_NOISE_TH_OFFSET                (76u)
#define CapSense_ADC_BUTTON2_NOISE_TH_SIZE                  (1u)
#define CapSense_ADC_BUTTON2_NOISE_TH_PARAM_ID              (0x4A82004Cu)

#define CapSense_ADC_BUTTON2_NNOISE_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button2.nNoiseTh)
#define CapSense_ADC_BUTTON2_NNOISE_TH_OFFSET               (77u)
#define CapSense_ADC_BUTTON2_NNOISE_TH_SIZE                 (1u)
#define CapSense_ADC_BUTTON2_NNOISE_TH_PARAM_ID             (0x4C82004Du)

#define CapSense_ADC_BUTTON2_HYSTERESIS_VALUE               (CapSense_ADC_dsRam.wdgtList.button2.hysteresis)
#define CapSense_ADC_BUTTON2_HYSTERESIS_OFFSET              (78u)
#define CapSense_ADC_BUTTON2_HYSTERESIS_SIZE                (1u)
#define CapSense_ADC_BUTTON2_HYSTERESIS_PARAM_ID            (0x4682004Eu)

#define CapSense_ADC_BUTTON2_ON_DEBOUNCE_VALUE              (CapSense_ADC_dsRam.wdgtList.button2.onDebounce)
#define CapSense_ADC_BUTTON2_ON_DEBOUNCE_OFFSET             (79u)
#define CapSense_ADC_BUTTON2_ON_DEBOUNCE_SIZE               (1u)
#define CapSense_ADC_BUTTON2_ON_DEBOUNCE_PARAM_ID           (0x4082004Fu)

#define CapSense_ADC_BUTTON2_LOW_BSLN_RST_VALUE             (CapSense_ADC_dsRam.wdgtList.button2.lowBslnRst)
#define CapSense_ADC_BUTTON2_LOW_BSLN_RST_OFFSET            (80u)
#define CapSense_ADC_BUTTON2_LOW_BSLN_RST_SIZE              (1u)
#define CapSense_ADC_BUTTON2_LOW_BSLN_RST_PARAM_ID          (0x4C820050u)

#define CapSense_ADC_BUTTON2_IDAC_MOD0_VALUE                (CapSense_ADC_dsRam.wdgtList.button2.idacMod[0u])
#define CapSense_ADC_BUTTON2_IDAC_MOD0_OFFSET               (81u)
#define CapSense_ADC_BUTTON2_IDAC_MOD0_SIZE                 (1u)
#define CapSense_ADC_BUTTON2_IDAC_MOD0_PARAM_ID             (0x47020051u)

#define CapSense_ADC_BUTTON2_SNS_CLK_VALUE                  (CapSense_ADC_dsRam.wdgtList.button2.snsClk)
#define CapSense_ADC_BUTTON2_SNS_CLK_OFFSET                 (82u)
#define CapSense_ADC_BUTTON2_SNS_CLK_SIZE                   (2u)
#define CapSense_ADC_BUTTON2_SNS_CLK_PARAM_ID               (0x88820052u)

#define CapSense_ADC_BUTTON2_SNS_CLK_SOURCE_VALUE           (CapSense_ADC_dsRam.wdgtList.button2.snsClkSource)
#define CapSense_ADC_BUTTON2_SNS_CLK_SOURCE_OFFSET          (84u)
#define CapSense_ADC_BUTTON2_SNS_CLK_SOURCE_SIZE            (1u)
#define CapSense_ADC_BUTTON2_SNS_CLK_SOURCE_PARAM_ID        (0x4D820054u)

#define CapSense_ADC_BUTTON2_FINGER_CAP_VALUE               (CapSense_ADC_dsRam.wdgtList.button2.fingerCap)
#define CapSense_ADC_BUTTON2_FINGER_CAP_OFFSET              (86u)
#define CapSense_ADC_BUTTON2_FINGER_CAP_SIZE                (2u)
#define CapSense_ADC_BUTTON2_FINGER_CAP_PARAM_ID            (0xAF020056u)

#define CapSense_ADC_BUTTON2_SIGPFC_VALUE                   (CapSense_ADC_dsRam.wdgtList.button2.sigPFC)
#define CapSense_ADC_BUTTON2_SIGPFC_OFFSET                  (88u)
#define CapSense_ADC_BUTTON2_SIGPFC_SIZE                    (2u)
#define CapSense_ADC_BUTTON2_SIGPFC_PARAM_ID                (0xA0020058u)

#define CapSense_ADC_BUTTON3_RESOLUTION_VALUE               (CapSense_ADC_dsRam.wdgtList.button3.resolution)
#define CapSense_ADC_BUTTON3_RESOLUTION_OFFSET              (90u)
#define CapSense_ADC_BUTTON3_RESOLUTION_SIZE                (2u)
#define CapSense_ADC_BUTTON3_RESOLUTION_PARAM_ID            (0x8983005Au)

#define CapSense_ADC_BUTTON3_FINGER_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button3.fingerTh)
#define CapSense_ADC_BUTTON3_FINGER_TH_OFFSET               (92u)
#define CapSense_ADC_BUTTON3_FINGER_TH_SIZE                 (2u)
#define CapSense_ADC_BUTTON3_FINGER_TH_PARAM_ID             (0x8483005Cu)

#define CapSense_ADC_BUTTON3_NOISE_TH_VALUE                 (CapSense_ADC_dsRam.wdgtList.button3.noiseTh)
#define CapSense_ADC_BUTTON3_NOISE_TH_OFFSET                (94u)
#define CapSense_ADC_BUTTON3_NOISE_TH_SIZE                  (1u)
#define CapSense_ADC_BUTTON3_NOISE_TH_PARAM_ID              (0x4083005Eu)

#define CapSense_ADC_BUTTON3_NNOISE_TH_VALUE                (CapSense_ADC_dsRam.wdgtList.button3.nNoiseTh)
#define CapSense_ADC_BUTTON3_NNOISE_TH_OFFSET               (95u)
#define CapSense_ADC_BUTTON3_NNOISE_TH_SIZE                 (1u)
#define CapSense_ADC_BUTTON3_NNOISE_TH_PARAM_ID             (0x4683005Fu)

#define CapSense_ADC_BUTTON3_HYSTERESIS_VALUE               (CapSense_ADC_dsRam.wdgtList.button3.hysteresis)
#define CapSense_ADC_BUTTON3_HYSTERESIS_OFFSET              (96u)
#define CapSense_ADC_BUTTON3_HYSTERESIS_SIZE                (1u)
#define CapSense_ADC_BUTTON3_HYSTERESIS_PARAM_ID            (0x40830060u)

#define CapSense_ADC_BUTTON3_ON_DEBOUNCE_VALUE              (CapSense_ADC_dsRam.wdgtList.button3.onDebounce)
#define CapSense_ADC_BUTTON3_ON_DEBOUNCE_OFFSET             (97u)
#define CapSense_ADC_BUTTON3_ON_DEBOUNCE_SIZE               (1u)
#define CapSense_ADC_BUTTON3_ON_DEBOUNCE_PARAM_ID           (0x46830061u)

#define CapSense_ADC_BUTTON3_LOW_BSLN_RST_VALUE             (CapSense_ADC_dsRam.wdgtList.button3.lowBslnRst)
#define CapSense_ADC_BUTTON3_LOW_BSLN_RST_OFFSET            (98u)
#define CapSense_ADC_BUTTON3_LOW_BSLN_RST_SIZE              (1u)
#define CapSense_ADC_BUTTON3_LOW_BSLN_RST_PARAM_ID          (0x4C830062u)

#define CapSense_ADC_BUTTON3_IDAC_MOD0_VALUE                (CapSense_ADC_dsRam.wdgtList.button3.idacMod[0u])
#define CapSense_ADC_BUTTON3_IDAC_MOD0_OFFSET               (99u)
#define CapSense_ADC_BUTTON3_IDAC_MOD0_SIZE                 (1u)
#define CapSense_ADC_BUTTON3_IDAC_MOD0_PARAM_ID             (0x47030063u)

#define CapSense_ADC_BUTTON3_SNS_CLK_VALUE                  (CapSense_ADC_dsRam.wdgtList.button3.snsClk)
#define CapSense_ADC_BUTTON3_SNS_CLK_OFFSET                 (100u)
#define CapSense_ADC_BUTTON3_SNS_CLK_SIZE                   (2u)
#define CapSense_ADC_BUTTON3_SNS_CLK_PARAM_ID               (0x89830064u)

#define CapSense_ADC_BUTTON3_SNS_CLK_SOURCE_VALUE           (CapSense_ADC_dsRam.wdgtList.button3.snsClkSource)
#define CapSense_ADC_BUTTON3_SNS_CLK_SOURCE_OFFSET          (102u)
#define CapSense_ADC_BUTTON3_SNS_CLK_SOURCE_SIZE            (1u)
#define CapSense_ADC_BUTTON3_SNS_CLK_SOURCE_PARAM_ID        (0x4D830066u)

#define CapSense_ADC_BUTTON3_FINGER_CAP_VALUE               (CapSense_ADC_dsRam.wdgtList.button3.fingerCap)
#define CapSense_ADC_BUTTON3_FINGER_CAP_OFFSET              (104u)
#define CapSense_ADC_BUTTON3_FINGER_CAP_SIZE                (2u)
#define CapSense_ADC_BUTTON3_FINGER_CAP_PARAM_ID            (0xAC030068u)

#define CapSense_ADC_BUTTON3_SIGPFC_VALUE                   (CapSense_ADC_dsRam.wdgtList.button3.sigPFC)
#define CapSense_ADC_BUTTON3_SIGPFC_OFFSET                  (106u)
#define CapSense_ADC_BUTTON3_SIGPFC_SIZE                    (2u)
#define CapSense_ADC_BUTTON3_SIGPFC_PARAM_ID                (0xA003006Au)

#define CapSense_ADC_BUTTON0_SNS0_RAW0_VALUE                (CapSense_ADC_dsRam.snsList.button0[0u].raw[0u])
#define CapSense_ADC_BUTTON0_SNS0_RAW0_OFFSET               (108u)
#define CapSense_ADC_BUTTON0_SNS0_RAW0_SIZE                 (2u)
#define CapSense_ADC_BUTTON0_SNS0_RAW0_PARAM_ID             (0x8300006Cu)

#define CapSense_ADC_BUTTON0_SNS0_BSLN0_VALUE               (CapSense_ADC_dsRam.snsList.button0[0u].bsln[0u])
#define CapSense_ADC_BUTTON0_SNS0_BSLN0_OFFSET              (110u)
#define CapSense_ADC_BUTTON0_SNS0_BSLN0_SIZE                (2u)
#define CapSense_ADC_BUTTON0_SNS0_BSLN0_PARAM_ID            (0x8F00006Eu)

#define CapSense_ADC_BUTTON0_SNS0_BSLN_EXT0_VALUE           (CapSense_ADC_dsRam.snsList.button0[0u].bslnExt[0u])
#define CapSense_ADC_BUTTON0_SNS0_BSLN_EXT0_OFFSET          (112u)
#define CapSense_ADC_BUTTON0_SNS0_BSLN_EXT0_SIZE            (1u)
#define CapSense_ADC_BUTTON0_SNS0_BSLN_EXT0_PARAM_ID        (0x4D000070u)

#define CapSense_ADC_BUTTON0_SNS0_DIFF_VALUE                (CapSense_ADC_dsRam.snsList.button0[0u].diff)
#define CapSense_ADC_BUTTON0_SNS0_DIFF_OFFSET               (114u)
#define CapSense_ADC_BUTTON0_SNS0_DIFF_SIZE                 (2u)
#define CapSense_ADC_BUTTON0_SNS0_DIFF_PARAM_ID             (0x89000072u)

#define CapSense_ADC_BUTTON0_SNS0_NEG_BSLN_RST_CNT0_VALUE   (CapSense_ADC_dsRam.snsList.button0[0u].negBslnRstCnt[0u])
#define CapSense_ADC_BUTTON0_SNS0_NEG_BSLN_RST_CNT0_OFFSET  (116u)
#define CapSense_ADC_BUTTON0_SNS0_NEG_BSLN_RST_CNT0_SIZE    (1u)
#define CapSense_ADC_BUTTON0_SNS0_NEG_BSLN_RST_CNT0_PARAM_ID (0x4C000074u)

#define CapSense_ADC_BUTTON0_SNS0_IDAC_COMP0_VALUE          (CapSense_ADC_dsRam.snsList.button0[0u].idacComp[0u])
#define CapSense_ADC_BUTTON0_SNS0_IDAC_COMP0_OFFSET         (117u)
#define CapSense_ADC_BUTTON0_SNS0_IDAC_COMP0_SIZE           (1u)
#define CapSense_ADC_BUTTON0_SNS0_IDAC_COMP0_PARAM_ID       (0x4A000075u)

#define CapSense_ADC_BUTTON1_SNS0_RAW0_VALUE                (CapSense_ADC_dsRam.snsList.button1[0u].raw[0u])
#define CapSense_ADC_BUTTON1_SNS0_RAW0_OFFSET               (118u)
#define CapSense_ADC_BUTTON1_SNS0_RAW0_SIZE                 (2u)
#define CapSense_ADC_BUTTON1_SNS0_RAW0_PARAM_ID             (0x88000076u)

#define CapSense_ADC_BUTTON1_SNS0_BSLN0_VALUE               (CapSense_ADC_dsRam.snsList.button1[0u].bsln[0u])
#define CapSense_ADC_BUTTON1_SNS0_BSLN0_OFFSET              (120u)
#define CapSense_ADC_BUTTON1_SNS0_BSLN0_SIZE                (2u)
#define CapSense_ADC_BUTTON1_SNS0_BSLN0_PARAM_ID            (0x87000078u)

#define CapSense_ADC_BUTTON1_SNS0_BSLN_EXT0_VALUE           (CapSense_ADC_dsRam.snsList.button1[0u].bslnExt[0u])
#define CapSense_ADC_BUTTON1_SNS0_BSLN_EXT0_OFFSET          (122u)
#define CapSense_ADC_BUTTON1_SNS0_BSLN_EXT0_SIZE            (1u)
#define CapSense_ADC_BUTTON1_SNS0_BSLN_EXT0_PARAM_ID        (0x4300007Au)

#define CapSense_ADC_BUTTON1_SNS0_DIFF_VALUE                (CapSense_ADC_dsRam.snsList.button1[0u].diff)
#define CapSense_ADC_BUTTON1_SNS0_DIFF_OFFSET               (124u)
#define CapSense_ADC_BUTTON1_SNS0_DIFF_SIZE                 (2u)
#define CapSense_ADC_BUTTON1_SNS0_DIFF_PARAM_ID             (0x8600007Cu)

#define CapSense_ADC_BUTTON1_SNS0_NEG_BSLN_RST_CNT0_VALUE   (CapSense_ADC_dsRam.snsList.button1[0u].negBslnRstCnt[0u])
#define CapSense_ADC_BUTTON1_SNS0_NEG_BSLN_RST_CNT0_OFFSET  (126u)
#define CapSense_ADC_BUTTON1_SNS0_NEG_BSLN_RST_CNT0_SIZE    (1u)
#define CapSense_ADC_BUTTON1_SNS0_NEG_BSLN_RST_CNT0_PARAM_ID (0x4200007Eu)

#define CapSense_ADC_BUTTON1_SNS0_IDAC_COMP0_VALUE          (CapSense_ADC_dsRam.snsList.button1[0u].idacComp[0u])
#define CapSense_ADC_BUTTON1_SNS0_IDAC_COMP0_OFFSET         (127u)
#define CapSense_ADC_BUTTON1_SNS0_IDAC_COMP0_SIZE           (1u)
#define CapSense_ADC_BUTTON1_SNS0_IDAC_COMP0_PARAM_ID       (0x4400007Fu)

#define CapSense_ADC_BUTTON2_SNS0_RAW0_VALUE                (CapSense_ADC_dsRam.snsList.button2[0u].raw[0u])
#define CapSense_ADC_BUTTON2_SNS0_RAW0_OFFSET               (128u)
#define CapSense_ADC_BUTTON2_SNS0_RAW0_SIZE                 (2u)
#define CapSense_ADC_BUTTON2_SNS0_RAW0_PARAM_ID             (0x84000080u)

#define CapSense_ADC_BUTTON2_SNS0_BSLN0_VALUE               (CapSense_ADC_dsRam.snsList.button2[0u].bsln[0u])
#define CapSense_ADC_BUTTON2_SNS0_BSLN0_OFFSET              (130u)
#define CapSense_ADC_BUTTON2_SNS0_BSLN0_SIZE                (2u)
#define CapSense_ADC_BUTTON2_SNS0_BSLN0_PARAM_ID            (0x88000082u)

#define CapSense_ADC_BUTTON2_SNS0_BSLN_EXT0_VALUE           (CapSense_ADC_dsRam.snsList.button2[0u].bslnExt[0u])
#define CapSense_ADC_BUTTON2_SNS0_BSLN_EXT0_OFFSET          (132u)
#define CapSense_ADC_BUTTON2_SNS0_BSLN_EXT0_SIZE            (1u)
#define CapSense_ADC_BUTTON2_SNS0_BSLN_EXT0_PARAM_ID        (0x4D000084u)

#define CapSense_ADC_BUTTON2_SNS0_DIFF_VALUE                (CapSense_ADC_dsRam.snsList.button2[0u].diff)
#define CapSense_ADC_BUTTON2_SNS0_DIFF_OFFSET               (134u)
#define CapSense_ADC_BUTTON2_SNS0_DIFF_SIZE                 (2u)
#define CapSense_ADC_BUTTON2_SNS0_DIFF_PARAM_ID             (0x89000086u)

#define CapSense_ADC_BUTTON2_SNS0_NEG_BSLN_RST_CNT0_VALUE   (CapSense_ADC_dsRam.snsList.button2[0u].negBslnRstCnt[0u])
#define CapSense_ADC_BUTTON2_SNS0_NEG_BSLN_RST_CNT0_OFFSET  (136u)
#define CapSense_ADC_BUTTON2_SNS0_NEG_BSLN_RST_CNT0_SIZE    (1u)
#define CapSense_ADC_BUTTON2_SNS0_NEG_BSLN_RST_CNT0_PARAM_ID (0x4E000088u)

#define CapSense_ADC_BUTTON2_SNS0_IDAC_COMP0_VALUE          (CapSense_ADC_dsRam.snsList.button2[0u].idacComp[0u])
#define CapSense_ADC_BUTTON2_SNS0_IDAC_COMP0_OFFSET         (137u)
#define CapSense_ADC_BUTTON2_SNS0_IDAC_COMP0_SIZE           (1u)
#define CapSense_ADC_BUTTON2_SNS0_IDAC_COMP0_PARAM_ID       (0x48000089u)

#define CapSense_ADC_BUTTON3_SNS0_RAW0_VALUE                (CapSense_ADC_dsRam.snsList.button3[0u].raw[0u])
#define CapSense_ADC_BUTTON3_SNS0_RAW0_OFFSET               (138u)
#define CapSense_ADC_BUTTON3_SNS0_RAW0_SIZE                 (2u)
#define CapSense_ADC_BUTTON3_SNS0_RAW0_PARAM_ID             (0x8A00008Au)

#define CapSense_ADC_BUTTON3_SNS0_BSLN0_VALUE               (CapSense_ADC_dsRam.snsList.button3[0u].bsln[0u])
#define CapSense_ADC_BUTTON3_SNS0_BSLN0_OFFSET              (140u)
#define CapSense_ADC_BUTTON3_SNS0_BSLN0_SIZE                (2u)
#define CapSense_ADC_BUTTON3_SNS0_BSLN0_PARAM_ID            (0x8700008Cu)

#define CapSense_ADC_BUTTON3_SNS0_BSLN_EXT0_VALUE           (CapSense_ADC_dsRam.snsList.button3[0u].bslnExt[0u])
#define CapSense_ADC_BUTTON3_SNS0_BSLN_EXT0_OFFSET          (142u)
#define CapSense_ADC_BUTTON3_SNS0_BSLN_EXT0_SIZE            (1u)
#define CapSense_ADC_BUTTON3_SNS0_BSLN_EXT0_PARAM_ID        (0x4300008Eu)

#define CapSense_ADC_BUTTON3_SNS0_DIFF_VALUE                (CapSense_ADC_dsRam.snsList.button3[0u].diff)
#define CapSense_ADC_BUTTON3_SNS0_DIFF_OFFSET               (144u)
#define CapSense_ADC_BUTTON3_SNS0_DIFF_SIZE                 (2u)
#define CapSense_ADC_BUTTON3_SNS0_DIFF_PARAM_ID             (0x81000090u)

#define CapSense_ADC_BUTTON3_SNS0_NEG_BSLN_RST_CNT0_VALUE   (CapSense_ADC_dsRam.snsList.button3[0u].negBslnRstCnt[0u])
#define CapSense_ADC_BUTTON3_SNS0_NEG_BSLN_RST_CNT0_OFFSET  (146u)
#define CapSense_ADC_BUTTON3_SNS0_NEG_BSLN_RST_CNT0_SIZE    (1u)
#define CapSense_ADC_BUTTON3_SNS0_NEG_BSLN_RST_CNT0_PARAM_ID (0x45000092u)

#define CapSense_ADC_BUTTON3_SNS0_IDAC_COMP0_VALUE          (CapSense_ADC_dsRam.snsList.button3[0u].idacComp[0u])
#define CapSense_ADC_BUTTON3_SNS0_IDAC_COMP0_OFFSET         (147u)
#define CapSense_ADC_BUTTON3_SNS0_IDAC_COMP0_SIZE           (1u)
#define CapSense_ADC_BUTTON3_SNS0_IDAC_COMP0_PARAM_ID       (0x43000093u)

#define CapSense_ADC_SNR_TEST_WIDGET_ID_VALUE               (CapSense_ADC_dsRam.snrTestWidgetId)
#define CapSense_ADC_SNR_TEST_WIDGET_ID_OFFSET              (148u)
#define CapSense_ADC_SNR_TEST_WIDGET_ID_SIZE                (1u)
#define CapSense_ADC_SNR_TEST_WIDGET_ID_PARAM_ID            (0x63000094u)

#define CapSense_ADC_SNR_TEST_SENSOR_ID_VALUE               (CapSense_ADC_dsRam.snrTestSensorId)
#define CapSense_ADC_SNR_TEST_SENSOR_ID_OFFSET              (149u)
#define CapSense_ADC_SNR_TEST_SENSOR_ID_SIZE                (1u)
#define CapSense_ADC_SNR_TEST_SENSOR_ID_PARAM_ID            (0x65000095u)

#define CapSense_ADC_SNR_TEST_SCAN_COUNTER_VALUE            (CapSense_ADC_dsRam.snrTestScanCounter)
#define CapSense_ADC_SNR_TEST_SCAN_COUNTER_OFFSET           (150u)
#define CapSense_ADC_SNR_TEST_SCAN_COUNTER_SIZE             (2u)
#define CapSense_ADC_SNR_TEST_SCAN_COUNTER_PARAM_ID         (0x8C000096u)

#define CapSense_ADC_SNR_TEST_RAW_COUNT0_VALUE              (CapSense_ADC_dsRam.snrTestRawCount[0u])
#define CapSense_ADC_SNR_TEST_RAW_COUNT0_OFFSET             (152u)
#define CapSense_ADC_SNR_TEST_RAW_COUNT0_SIZE               (2u)
#define CapSense_ADC_SNR_TEST_RAW_COUNT0_PARAM_ID           (0x83000098u)


/*****************************************************************************/
/* Flash Data structure register definitions                                 */
/*****************************************************************************/
#define CapSense_ADC_BUTTON0_PTR2SNS_FLASH_VALUE            (CapSense_ADC_dsFlash.wdgtArray[0].ptr2SnsFlash)
#define CapSense_ADC_BUTTON0_PTR2SNS_FLASH_OFFSET           (0u)
#define CapSense_ADC_BUTTON0_PTR2SNS_FLASH_SIZE             (4u)
#define CapSense_ADC_BUTTON0_PTR2SNS_FLASH_PARAM_ID         (0xD1000000u)

#define CapSense_ADC_BUTTON0_PTR2WD_RAM_VALUE               (CapSense_ADC_dsFlash.wdgtArray[0].ptr2WdgtRam)
#define CapSense_ADC_BUTTON0_PTR2WD_RAM_OFFSET              (4u)
#define CapSense_ADC_BUTTON0_PTR2WD_RAM_SIZE                (4u)
#define CapSense_ADC_BUTTON0_PTR2WD_RAM_PARAM_ID            (0xD0000004u)

#define CapSense_ADC_BUTTON0_PTR2SNS_RAM_VALUE              (CapSense_ADC_dsFlash.wdgtArray[0].ptr2SnsRam)
#define CapSense_ADC_BUTTON0_PTR2SNS_RAM_OFFSET             (8u)
#define CapSense_ADC_BUTTON0_PTR2SNS_RAM_SIZE               (4u)
#define CapSense_ADC_BUTTON0_PTR2SNS_RAM_PARAM_ID           (0xD3000008u)

#define CapSense_ADC_BUTTON0_PTR2FLTR_HISTORY_VALUE         (CapSense_ADC_dsFlash.wdgtArray[0].ptr2FltrHistory)
#define CapSense_ADC_BUTTON0_PTR2FLTR_HISTORY_OFFSET        (12u)
#define CapSense_ADC_BUTTON0_PTR2FLTR_HISTORY_SIZE          (4u)
#define CapSense_ADC_BUTTON0_PTR2FLTR_HISTORY_PARAM_ID      (0xD200000Cu)

#define CapSense_ADC_BUTTON0_PTR2DEBOUNCE_VALUE             (CapSense_ADC_dsFlash.wdgtArray[0].ptr2DebounceArr)
#define CapSense_ADC_BUTTON0_PTR2DEBOUNCE_OFFSET            (16u)
#define CapSense_ADC_BUTTON0_PTR2DEBOUNCE_SIZE              (4u)
#define CapSense_ADC_BUTTON0_PTR2DEBOUNCE_PARAM_ID          (0xD4000010u)

#define CapSense_ADC_BUTTON0_STATIC_CONFIG_VALUE            (CapSense_ADC_dsFlash.wdgtArray[0].staticConfig)
#define CapSense_ADC_BUTTON0_STATIC_CONFIG_OFFSET           (20u)
#define CapSense_ADC_BUTTON0_STATIC_CONFIG_SIZE             (2u)
#define CapSense_ADC_BUTTON0_STATIC_CONFIG_PARAM_ID         (0x9A000014u)

#define CapSense_ADC_BUTTON0_TOTAL_NUM_SNS_VALUE            (CapSense_ADC_dsFlash.wdgtArray[0].totalNumSns)
#define CapSense_ADC_BUTTON0_TOTAL_NUM_SNS_OFFSET           (22u)
#define CapSense_ADC_BUTTON0_TOTAL_NUM_SNS_SIZE             (2u)
#define CapSense_ADC_BUTTON0_TOTAL_NUM_SNS_PARAM_ID         (0x96000016u)

#define CapSense_ADC_BUTTON0_TYPE_VALUE                     (CapSense_ADC_dsFlash.wdgtArray[0].wdgtType)
#define CapSense_ADC_BUTTON0_TYPE_OFFSET                    (24u)
#define CapSense_ADC_BUTTON0_TYPE_SIZE                      (1u)
#define CapSense_ADC_BUTTON0_TYPE_PARAM_ID                  (0x51000018u)

#define CapSense_ADC_BUTTON0_NUM_COLS_VALUE                 (CapSense_ADC_dsFlash.wdgtArray[0].numCols)
#define CapSense_ADC_BUTTON0_NUM_COLS_OFFSET                (25u)
#define CapSense_ADC_BUTTON0_NUM_COLS_SIZE                  (1u)
#define CapSense_ADC_BUTTON0_NUM_COLS_PARAM_ID              (0x57000019u)

#define CapSense_ADC_BUTTON0_PTR2NOISE_ENVLP_VALUE          (CapSense_ADC_dsFlash.wdgtArray[0].ptr2NoiseEnvlp)
#define CapSense_ADC_BUTTON0_PTR2NOISE_ENVLP_OFFSET         (28u)
#define CapSense_ADC_BUTTON0_PTR2NOISE_ENVLP_SIZE           (4u)
#define CapSense_ADC_BUTTON0_PTR2NOISE_ENVLP_PARAM_ID       (0xD700001Cu)

#define CapSense_ADC_BUTTON1_PTR2SNS_FLASH_VALUE            (CapSense_ADC_dsFlash.wdgtArray[1].ptr2SnsFlash)
#define CapSense_ADC_BUTTON1_PTR2SNS_FLASH_OFFSET           (32u)
#define CapSense_ADC_BUTTON1_PTR2SNS_FLASH_SIZE             (4u)
#define CapSense_ADC_BUTTON1_PTR2SNS_FLASH_PARAM_ID         (0xD8010020u)

#define CapSense_ADC_BUTTON1_PTR2WD_RAM_VALUE               (CapSense_ADC_dsFlash.wdgtArray[1].ptr2WdgtRam)
#define CapSense_ADC_BUTTON1_PTR2WD_RAM_OFFSET              (36u)
#define CapSense_ADC_BUTTON1_PTR2WD_RAM_SIZE                (4u)
#define CapSense_ADC_BUTTON1_PTR2WD_RAM_PARAM_ID            (0xD9010024u)

#define CapSense_ADC_BUTTON1_PTR2SNS_RAM_VALUE              (CapSense_ADC_dsFlash.wdgtArray[1].ptr2SnsRam)
#define CapSense_ADC_BUTTON1_PTR2SNS_RAM_OFFSET             (40u)
#define CapSense_ADC_BUTTON1_PTR2SNS_RAM_SIZE               (4u)
#define CapSense_ADC_BUTTON1_PTR2SNS_RAM_PARAM_ID           (0xDA010028u)

#define CapSense_ADC_BUTTON1_PTR2FLTR_HISTORY_VALUE         (CapSense_ADC_dsFlash.wdgtArray[1].ptr2FltrHistory)
#define CapSense_ADC_BUTTON1_PTR2FLTR_HISTORY_OFFSET        (44u)
#define CapSense_ADC_BUTTON1_PTR2FLTR_HISTORY_SIZE          (4u)
#define CapSense_ADC_BUTTON1_PTR2FLTR_HISTORY_PARAM_ID      (0xDB01002Cu)

#define CapSense_ADC_BUTTON1_PTR2DEBOUNCE_VALUE             (CapSense_ADC_dsFlash.wdgtArray[1].ptr2DebounceArr)
#define CapSense_ADC_BUTTON1_PTR2DEBOUNCE_OFFSET            (48u)
#define CapSense_ADC_BUTTON1_PTR2DEBOUNCE_SIZE              (4u)
#define CapSense_ADC_BUTTON1_PTR2DEBOUNCE_PARAM_ID          (0xDD010030u)

#define CapSense_ADC_BUTTON1_STATIC_CONFIG_VALUE            (CapSense_ADC_dsFlash.wdgtArray[1].staticConfig)
#define CapSense_ADC_BUTTON1_STATIC_CONFIG_OFFSET           (52u)
#define CapSense_ADC_BUTTON1_STATIC_CONFIG_SIZE             (2u)
#define CapSense_ADC_BUTTON1_STATIC_CONFIG_PARAM_ID         (0x93010034u)

#define CapSense_ADC_BUTTON1_TOTAL_NUM_SNS_VALUE            (CapSense_ADC_dsFlash.wdgtArray[1].totalNumSns)
#define CapSense_ADC_BUTTON1_TOTAL_NUM_SNS_OFFSET           (54u)
#define CapSense_ADC_BUTTON1_TOTAL_NUM_SNS_SIZE             (2u)
#define CapSense_ADC_BUTTON1_TOTAL_NUM_SNS_PARAM_ID         (0x9F010036u)

#define CapSense_ADC_BUTTON1_TYPE_VALUE                     (CapSense_ADC_dsFlash.wdgtArray[1].wdgtType)
#define CapSense_ADC_BUTTON1_TYPE_OFFSET                    (56u)
#define CapSense_ADC_BUTTON1_TYPE_SIZE                      (1u)
#define CapSense_ADC_BUTTON1_TYPE_PARAM_ID                  (0x58010038u)

#define CapSense_ADC_BUTTON1_NUM_COLS_VALUE                 (CapSense_ADC_dsFlash.wdgtArray[1].numCols)
#define CapSense_ADC_BUTTON1_NUM_COLS_OFFSET                (57u)
#define CapSense_ADC_BUTTON1_NUM_COLS_SIZE                  (1u)
#define CapSense_ADC_BUTTON1_NUM_COLS_PARAM_ID              (0x5E010039u)

#define CapSense_ADC_BUTTON1_PTR2NOISE_ENVLP_VALUE          (CapSense_ADC_dsFlash.wdgtArray[1].ptr2NoiseEnvlp)
#define CapSense_ADC_BUTTON1_PTR2NOISE_ENVLP_OFFSET         (60u)
#define CapSense_ADC_BUTTON1_PTR2NOISE_ENVLP_SIZE           (4u)
#define CapSense_ADC_BUTTON1_PTR2NOISE_ENVLP_PARAM_ID       (0xDE01003Cu)

#define CapSense_ADC_BUTTON2_PTR2SNS_FLASH_VALUE            (CapSense_ADC_dsFlash.wdgtArray[2].ptr2SnsFlash)
#define CapSense_ADC_BUTTON2_PTR2SNS_FLASH_OFFSET           (64u)
#define CapSense_ADC_BUTTON2_PTR2SNS_FLASH_SIZE             (4u)
#define CapSense_ADC_BUTTON2_PTR2SNS_FLASH_PARAM_ID         (0xDA020040u)

#define CapSense_ADC_BUTTON2_PTR2WD_RAM_VALUE               (CapSense_ADC_dsFlash.wdgtArray[2].ptr2WdgtRam)
#define CapSense_ADC_BUTTON2_PTR2WD_RAM_OFFSET              (68u)
#define CapSense_ADC_BUTTON2_PTR2WD_RAM_SIZE                (4u)
#define CapSense_ADC_BUTTON2_PTR2WD_RAM_PARAM_ID            (0xDB020044u)

#define CapSense_ADC_BUTTON2_PTR2SNS_RAM_VALUE              (CapSense_ADC_dsFlash.wdgtArray[2].ptr2SnsRam)
#define CapSense_ADC_BUTTON2_PTR2SNS_RAM_OFFSET             (72u)
#define CapSense_ADC_BUTTON2_PTR2SNS_RAM_SIZE               (4u)
#define CapSense_ADC_BUTTON2_PTR2SNS_RAM_PARAM_ID           (0xD8020048u)

#define CapSense_ADC_BUTTON2_PTR2FLTR_HISTORY_VALUE         (CapSense_ADC_dsFlash.wdgtArray[2].ptr2FltrHistory)
#define CapSense_ADC_BUTTON2_PTR2FLTR_HISTORY_OFFSET        (76u)
#define CapSense_ADC_BUTTON2_PTR2FLTR_HISTORY_SIZE          (4u)
#define CapSense_ADC_BUTTON2_PTR2FLTR_HISTORY_PARAM_ID      (0xD902004Cu)

#define CapSense_ADC_BUTTON2_PTR2DEBOUNCE_VALUE             (CapSense_ADC_dsFlash.wdgtArray[2].ptr2DebounceArr)
#define CapSense_ADC_BUTTON2_PTR2DEBOUNCE_OFFSET            (80u)
#define CapSense_ADC_BUTTON2_PTR2DEBOUNCE_SIZE              (4u)
#define CapSense_ADC_BUTTON2_PTR2DEBOUNCE_PARAM_ID          (0xDF020050u)

#define CapSense_ADC_BUTTON2_STATIC_CONFIG_VALUE            (CapSense_ADC_dsFlash.wdgtArray[2].staticConfig)
#define CapSense_ADC_BUTTON2_STATIC_CONFIG_OFFSET           (84u)
#define CapSense_ADC_BUTTON2_STATIC_CONFIG_SIZE             (2u)
#define CapSense_ADC_BUTTON2_STATIC_CONFIG_PARAM_ID         (0x91020054u)

#define CapSense_ADC_BUTTON2_TOTAL_NUM_SNS_VALUE            (CapSense_ADC_dsFlash.wdgtArray[2].totalNumSns)
#define CapSense_ADC_BUTTON2_TOTAL_NUM_SNS_OFFSET           (86u)
#define CapSense_ADC_BUTTON2_TOTAL_NUM_SNS_SIZE             (2u)
#define CapSense_ADC_BUTTON2_TOTAL_NUM_SNS_PARAM_ID         (0x9D020056u)

#define CapSense_ADC_BUTTON2_TYPE_VALUE                     (CapSense_ADC_dsFlash.wdgtArray[2].wdgtType)
#define CapSense_ADC_BUTTON2_TYPE_OFFSET                    (88u)
#define CapSense_ADC_BUTTON2_TYPE_SIZE                      (1u)
#define CapSense_ADC_BUTTON2_TYPE_PARAM_ID                  (0x5A020058u)

#define CapSense_ADC_BUTTON2_NUM_COLS_VALUE                 (CapSense_ADC_dsFlash.wdgtArray[2].numCols)
#define CapSense_ADC_BUTTON2_NUM_COLS_OFFSET                (89u)
#define CapSense_ADC_BUTTON2_NUM_COLS_SIZE                  (1u)
#define CapSense_ADC_BUTTON2_NUM_COLS_PARAM_ID              (0x5C020059u)

#define CapSense_ADC_BUTTON2_PTR2NOISE_ENVLP_VALUE          (CapSense_ADC_dsFlash.wdgtArray[2].ptr2NoiseEnvlp)
#define CapSense_ADC_BUTTON2_PTR2NOISE_ENVLP_OFFSET         (92u)
#define CapSense_ADC_BUTTON2_PTR2NOISE_ENVLP_SIZE           (4u)
#define CapSense_ADC_BUTTON2_PTR2NOISE_ENVLP_PARAM_ID       (0xDC02005Cu)

#define CapSense_ADC_BUTTON3_PTR2SNS_FLASH_VALUE            (CapSense_ADC_dsFlash.wdgtArray[3].ptr2SnsFlash)
#define CapSense_ADC_BUTTON3_PTR2SNS_FLASH_OFFSET           (96u)
#define CapSense_ADC_BUTTON3_PTR2SNS_FLASH_SIZE             (4u)
#define CapSense_ADC_BUTTON3_PTR2SNS_FLASH_PARAM_ID         (0xD3030060u)

#define CapSense_ADC_BUTTON3_PTR2WD_RAM_VALUE               (CapSense_ADC_dsFlash.wdgtArray[3].ptr2WdgtRam)
#define CapSense_ADC_BUTTON3_PTR2WD_RAM_OFFSET              (100u)
#define CapSense_ADC_BUTTON3_PTR2WD_RAM_SIZE                (4u)
#define CapSense_ADC_BUTTON3_PTR2WD_RAM_PARAM_ID            (0xD2030064u)

#define CapSense_ADC_BUTTON3_PTR2SNS_RAM_VALUE              (CapSense_ADC_dsFlash.wdgtArray[3].ptr2SnsRam)
#define CapSense_ADC_BUTTON3_PTR2SNS_RAM_OFFSET             (104u)
#define CapSense_ADC_BUTTON3_PTR2SNS_RAM_SIZE               (4u)
#define CapSense_ADC_BUTTON3_PTR2SNS_RAM_PARAM_ID           (0xD1030068u)

#define CapSense_ADC_BUTTON3_PTR2FLTR_HISTORY_VALUE         (CapSense_ADC_dsFlash.wdgtArray[3].ptr2FltrHistory)
#define CapSense_ADC_BUTTON3_PTR2FLTR_HISTORY_OFFSET        (108u)
#define CapSense_ADC_BUTTON3_PTR2FLTR_HISTORY_SIZE          (4u)
#define CapSense_ADC_BUTTON3_PTR2FLTR_HISTORY_PARAM_ID      (0xD003006Cu)

#define CapSense_ADC_BUTTON3_PTR2DEBOUNCE_VALUE             (CapSense_ADC_dsFlash.wdgtArray[3].ptr2DebounceArr)
#define CapSense_ADC_BUTTON3_PTR2DEBOUNCE_OFFSET            (112u)
#define CapSense_ADC_BUTTON3_PTR2DEBOUNCE_SIZE              (4u)
#define CapSense_ADC_BUTTON3_PTR2DEBOUNCE_PARAM_ID          (0xD6030070u)

#define CapSense_ADC_BUTTON3_STATIC_CONFIG_VALUE            (CapSense_ADC_dsFlash.wdgtArray[3].staticConfig)
#define CapSense_ADC_BUTTON3_STATIC_CONFIG_OFFSET           (116u)
#define CapSense_ADC_BUTTON3_STATIC_CONFIG_SIZE             (2u)
#define CapSense_ADC_BUTTON3_STATIC_CONFIG_PARAM_ID         (0x98030074u)

#define CapSense_ADC_BUTTON3_TOTAL_NUM_SNS_VALUE            (CapSense_ADC_dsFlash.wdgtArray[3].totalNumSns)
#define CapSense_ADC_BUTTON3_TOTAL_NUM_SNS_OFFSET           (118u)
#define CapSense_ADC_BUTTON3_TOTAL_NUM_SNS_SIZE             (2u)
#define CapSense_ADC_BUTTON3_TOTAL_NUM_SNS_PARAM_ID         (0x94030076u)

#define CapSense_ADC_BUTTON3_TYPE_VALUE                     (CapSense_ADC_dsFlash.wdgtArray[3].wdgtType)
#define CapSense_ADC_BUTTON3_TYPE_OFFSET                    (120u)
#define CapSense_ADC_BUTTON3_TYPE_SIZE                      (1u)
#define CapSense_ADC_BUTTON3_TYPE_PARAM_ID                  (0x53030078u)

#define CapSense_ADC_BUTTON3_NUM_COLS_VALUE                 (CapSense_ADC_dsFlash.wdgtArray[3].numCols)
#define CapSense_ADC_BUTTON3_NUM_COLS_OFFSET                (121u)
#define CapSense_ADC_BUTTON3_NUM_COLS_SIZE                  (1u)
#define CapSense_ADC_BUTTON3_NUM_COLS_PARAM_ID              (0x55030079u)

#define CapSense_ADC_BUTTON3_PTR2NOISE_ENVLP_VALUE          (CapSense_ADC_dsFlash.wdgtArray[3].ptr2NoiseEnvlp)
#define CapSense_ADC_BUTTON3_PTR2NOISE_ENVLP_OFFSET         (124u)
#define CapSense_ADC_BUTTON3_PTR2NOISE_ENVLP_SIZE           (4u)
#define CapSense_ADC_BUTTON3_PTR2NOISE_ENVLP_PARAM_ID       (0xD503007Cu)


#endif /* End CY_CAPSENSE_CapSense_ADC_REGISTER_MAP_H */

/* [] END OF FILE */

/*
 * UART_User.h
 *
 *  Created on: Nov 2, 2019
 *      Author: chungnguyen
 */

#ifndef UART_USER_H_
#define UART_USER_H_
#include "Cpu.h"
#include "UART.h"

#define MAX_LENGH_BUFFER 100

typedef struct
{
	uint8_t buffer[MAX_LENGH_BUFFER];
	uint8_t count;
	uint8_t dataByte;
	uint8_t state;
} uart_typedef_t;

void UART_UserInit(void);
void UART_UserReceiveIT(void);
void UART_UserProcess(void);
#endif /* UART_USER_H_ */
